import React from 'react';
const SingleProduct = ({
  product
}) => {
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "border shadow-slate-100 border-black rounded-lg px-2 py-3"
  }, /*#__PURE__*/React.createElement("a", null, /*#__PURE__*/React.createElement("div", {
    className: " w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7"
  }, /*#__PURE__*/React.createElement("img", {
    src: product.images[0],
    alt: "productimg",
    className: "h-full w-full object-cover object-center "
  })), /*#__PURE__*/React.createElement("h3", {
    className: "mt-4 text-sm text-gray-700"
  }, product.title), /*#__PURE__*/React.createElement("p", {
    className: "mt-1 text-lg font-medium text-gray-900"
  }, "\u20B9549"))));
};
export default SingleProduct;