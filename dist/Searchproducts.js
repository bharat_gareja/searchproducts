import React, { useState, useEffect } from 'react';
import SingleProduct from './SingleProduct';
import axios from 'axios';
const Searchproducts = () => {
  const [inputText, setInputText] = useState('');
  const [data, setData] = useState([]);
  useEffect(() => {
    axios.get('https://dummyjson.com/products').then(response => {
      setData(response.data.products);
    });
    const getData = setTimeout(() => {
      axios.get(`https://dummyjson.com/products/search?q=${inputText}`).then(res => {
        setData(res.data.products);
      });
    }, 1500);
    return () => clearTimeout(getData);
  }, [inputText]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "flex justify-center py-6 items-center"
  }, /*#__PURE__*/React.createElement("div", {
    className: "flex flex-col items-center"
  }, /*#__PURE__*/React.createElement("h1", {
    className: "text-lg font-semibold"
  }, "Search Product"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
    type: "text",
    className: "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block px-2 py-2",
    onChange: event => setInputText(event.target.value)
  })))), /*#__PURE__*/React.createElement("div", {
    className: "grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8 px-10"
  }, data.map(product => /*#__PURE__*/React.createElement(SingleProduct, {
    product: product,
    key: product.id
  }))));
};
export default Searchproducts;