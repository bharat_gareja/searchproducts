import React from 'react';

const SingleProduct = ({ product }) => {
  return (
    <>
      <div className='border shadow-slate-100 border-black rounded-lg px-2 py-3'>
        <a>
          <div className=' w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7'>
            <img src={product.images[0]} alt='productimg' className='h-full w-full object-cover object-center ' />
          </div>
          <h3 className='mt-4 text-sm text-gray-700'>{product.title}</h3>
          <p className='mt-1 text-lg font-medium text-gray-900'>&#8377;549</p>
        </a>
      </div>
    </>
  );
};

export default SingleProduct;

