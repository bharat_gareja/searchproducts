import React, { useEffect, useState } from 'react';
import axios from 'axios';
const Search = () => {
  const [pinCode, setPinCode] = useState('');

  useEffect(() => {
    const getData = setTimeout(() => {
      axios.get(`https://api.postalpincode.in/pincode/${pinCode}`).then((res) => {
        console.log(res.data[0]?.PostOffice[0]);
      });
    }, 2000);
    return () => clearTimeout(getData);
  }, [pinCode]);

  return (
    <>
      <div style={{ textAlign: 'center' }}>
        <h1>search</h1>
        <div>
          <form>
            <label>Search</label>
            <input type='text' placeholder='Search Input..' onChange={(event) => setPinCode(event.target.value)} />
          </form>
        </div>
      </div>
    </>
  );
};

export default Search;
