import React, { useEffect, useState } from 'react';
import axios from 'axios';
const Search = () => {
  const [pinCode, setPinCode] = useState('');
  useEffect(() => {
    const getData = setTimeout(() => {
      axios.get(`https://api.postalpincode.in/pincode/${pinCode}`).then(res => {
        console.log(res.data[0]?.PostOffice[0]);
      });
    }, 2000);
    return () => clearTimeout(getData);
  }, [pinCode]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    style: {
      textAlign: 'center'
    }
  }, /*#__PURE__*/React.createElement("h1", null, "search"), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("form", null, /*#__PURE__*/React.createElement("label", null, "Search"), /*#__PURE__*/React.createElement("input", {
    type: "text",
    placeholder: "Search Input..",
    onChange: event => setPinCode(event.target.value)
  })))));
};
export default Search;